#ifndef STUDENTS_LIST_H_
#define STUDENTS_LIST_H_

#include <stdbool.h>
#include "utils/linked_list.h"
#include "students_info.h"

typedef struct __students_list_ {
	linked_list_t list;
}students_list_t;

void student_init(students_list_t* slist);
bool student_push(students_list_t* slist,student_info_t *student);
bool student_push_to_front(students_list_t* slist,student_info_t *student);
bool student_insert(students_list_t* slist,student_info_t *student,uint16_t position);

student_info_t* student_pop(students_list_t* slist);
student_info_t* student_pop_by_position(students_list_t* slist,uint16_t position);
student_info_t* student_peek(students_list_t* slist);
uint16_t student_get_size(students_list_t* slist);
void student_print_details(students_list_t* slist);

#endif /* STUDENTS_LIST_H_ */
