#ifndef STUDENTS_STACK_H_
#define STUDENTS_STACK_H_

#include <stdbool.h>
#include "students_info.h"

typedef struct __students_stack_ {
	linked_list_t list;
}students_stack_t;

void student_stack_init(students_stack_t* stack);
bool student_stack_push(students_stack_t* stack,student_info_t *student);

student_info_t* student_stack_pop(students_stack_t* stack);
student_info_t* student_stack_peek(students_stack_t* stack);
uint16_t student_stack_get_size(students_stack_t* stack);
void student_stack_print_details(students_stack_t* stack);


#endif /* STUDENTS_STACK_H_ */
