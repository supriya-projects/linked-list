#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "students_stack.h"
#include "utils/linked_list.h"

static void _print_item_details(void* parent);

void student_stack_init(students_stack_t* stack)
{
	if(stack == NULL)
		return;

	linked_list_init(&stack->list);
}

bool student_stack_push(students_stack_t* stack,student_info_t *student)
{
	if(stack == NULL)
		return false;

	if(!student)
		return false;

	student->item.parent = (void *)student;

	return linked_list_push_to_front(&stack->list, &student->item);
}


student_info_t* student_stack_pop(students_stack_t* stack)
{
	if(stack == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_pop(&stack->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}

student_info_t* student_stack_peek(students_stack_t* stack)
{
	if(stack == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_peek(&stack->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}

uint16_t student_stack_get_size(students_stack_t* stack)
{
	if(stack == NULL)
		return 0;

	return stack->list.size;
}

void student_stack_print_details(students_stack_t* stack)
{
	if(stack == NULL)
		return;

	linked_list_print_details(&stack->list, _print_item_details);
}

static void _print_item_details(void* parent)
{
	student_info_t* temp;

	if(!parent)
		return;

	temp = (student_info_t*)parent;
	printf("%4d, %s\n",temp->id,temp->name);

}
