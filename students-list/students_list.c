#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "students_list.h"
#include "utils/linked_list.h"

static void _print_item_details(void* parent);

void student_init(students_list_t* slist)
{
	if(slist == NULL)
		return;

	linked_list_init(&slist->list);
}

bool student_push(students_list_t* slist,student_info_t *student)
{
	if(slist == NULL)
		return false;

	if(!student)
		return false;

	student->item.parent = (void *)student;

	return linked_list_push(&slist->list, &student->item);

}

bool student_push_to_front(students_list_t* slist,student_info_t *student)
{
	if(slist == NULL)
		return false;

	if(!student)
		return false;

	student->item.parent = (void *)student;

	return linked_list_push_to_front(&slist->list, &student->item);

}

bool student_insert(students_list_t* slist,student_info_t *student,uint16_t position)
{
	if(slist == NULL)
		return false;

	if(!student)
		return false;

	student->item.parent = (void *)student;

	return linked_list_insert(&slist->list, &student->item, position);

}


student_info_t* student_pop(students_list_t* slist)
{
	if(slist == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_pop(&slist->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}

student_info_t* student_pop_by_position(students_list_t* slist,uint16_t position)
{
	if(slist == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_pop_by_position(&slist->list,position);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}

student_info_t* student_peek(students_list_t* slist)
{
	if(slist == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_peek(&slist->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}

uint16_t student_get_size(students_list_t* slist)
{
	if(slist == NULL)
		return 0;

	return slist->list.size;
}

void student_print_details(students_list_t* slist)
{
	if(slist == NULL)
		return;

	linked_list_print_details(&slist->list, _print_item_details);
}

static void _print_item_details(void* parent)
{
	student_info_t* temp;

	if(!parent)
		return;

	temp = (student_info_t*)parent;
	printf("%4d, %s\n",temp->id,temp->name);

}
