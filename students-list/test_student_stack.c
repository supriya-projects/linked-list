#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "test_student_stack.h"
#include "students_stack.h"
#include "students_info.h"

#define MAX_STUDENTS 10

static student_info_t __student_info[MAX_STUDENTS];
static students_stack_t __volunteers_pool;
static students_stack_t __volunteers;

static uint8_t __roll_number;

static void __initialize_lists(void);
static void __print_list_size(void);
static void __add_student(void);
static void __clear_list(void);

void test_student_stack(void)
{
	__initialize_lists();
	__roll_number = 10;
	__add_student();
	__add_student();
	__add_student();
	__add_student();

	__print_list_size();
	student_stack_print_details(&__volunteers);

	__clear_list();
	__print_list_size();
}

static void __add_student(void)
{
	/*
	 * logic
	 * take empty structure from pool
	 * fill the empty structure with data
	 * push filled structure in the list
	 */
	student_info_t* temp;
	bool result;

	temp = student_stack_pop(&__volunteers_pool);
	if(temp == NULL){
		printf("no buffer available in pool.cannot add student\n");
		return;
	}

	//fill struct with info
	temp->id = __roll_number;
	strcpy((char*)temp->name,"test");
	__roll_number++;

	//push filled struct in list
	result = student_stack_push(&__volunteers, temp);
	if(result == false){
		printf("failed to add student to list\n");
		//put back struct in the pool
		result = student_stack_push(&__volunteers_pool, temp);
		if(result==false)
			printf("failed to add struct back in the pool,there will be loss of structure\n");
	}
}

static void __initialize_lists(void)
{
	//init both lists
	student_stack_init(&__volunteers_pool);
	student_stack_init(&__volunteers);

	for(uint8_t i = 0; i < MAX_STUDENTS; i++){
		student_stack_push(&__volunteers_pool, &__student_info[i]);
	}
	__print_list_size();
}

static void __print_list_size(void)
{
	printf("pool size: %2d, list size: %2d\n"
			,student_stack_get_size(&__volunteers_pool)
			,student_stack_get_size(&__volunteers));

}

static void __clear_list(void)
{
	student_info_t* temp;

	temp = student_stack_pop(&__volunteers);
	while(temp != NULL){
		student_stack_push(&__volunteers_pool, temp);
		temp = student_stack_pop(&__volunteers);
	}
}
