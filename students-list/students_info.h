#ifndef STUDENTS_INFO_H_
#define STUDENTS_INFO_H_

#include "utils/linked_list.h"

#define STUDENT_NAME_MAX_SIZE_BYTES 100

typedef struct __student_info_ {
	uint16_t id;
	uint8_t name[STUDENT_NAME_MAX_SIZE_BYTES];
	linked_list_item_t item;
}student_info_t;


#endif /* STUDENTS_INFO_H_ */
