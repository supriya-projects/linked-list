#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "students_list.h"
#include "test_student_stack.h"

#define MAX_STUDENTS 10

static student_info_t __student_info[MAX_STUDENTS];
static students_list_t __ece_students_pool;
static students_list_t __ece_students;

static uint8_t __roll_number;

static void __initialize_lists(void);
static void __print_list_size(void);
static void __add_student(void);
static void __clear_list(void);

int main(void)
{
	//test stack
	test_student_stack();
//	return 0;

	__initialize_lists();
	__roll_number = 10;
	__add_student();
	__add_student();

	__roll_number = 15;
	__add_student();
	__add_student();

	__print_list_size();
	student_print_details(&__ece_students);

	__clear_list();
	__print_list_size();

	return 0;
}

static void __add_student(void)
{
	/*
	 * logic
	 * take empty structure from pool
	 * fill the empty structure with data
	 * push filled structure in the list
	 */
	student_info_t* temp;
	bool result;

	temp = student_pop(&__ece_students_pool);
	if(temp == NULL){
		printf("no buffer available in pool.cannot add student\n");
		return;
	}

	//fill struct with info
	temp->id = __roll_number;
	strcpy((char*)temp->name,"test");
	__roll_number++;

	//push filled struct in list
	result = student_push(&__ece_students, temp);
	if(result == false){
		printf("failed to add student to list\n");
		//put back struct in the pool
		result = student_push(&__ece_students_pool, temp);
		if(result==false)
			printf("failed to add struct back in the pool,there will be loss of structure\n");
	}
}

static void __initialize_lists(void)
{
	//init both lists
	student_init(&__ece_students_pool);
	student_init(&__ece_students);

	for(uint8_t i = 0; i < MAX_STUDENTS; i++){
		student_push(&__ece_students_pool, &__student_info[i]);
	}
	__print_list_size();
}

static void __print_list_size(void)
{
	printf("pool size: %2d, list size: %2d\n"
			,student_get_size(&__ece_students_pool)
			,student_get_size(&__ece_students));

}

static void __clear_list(void)
{
	student_info_t* temp;

	temp = student_pop(&__ece_students);
	while(temp != NULL){
		student_push(&__ece_students_pool, temp);
		temp = student_pop(&__ece_students);
	}
}
