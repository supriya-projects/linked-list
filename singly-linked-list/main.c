#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "students.h"
#include "course.h"

static void _add_test_data(void);
static void _clear_data(void);
static void _insert_data(void);

static void _course_details(linked_list_t* list);
static void _course_add_test_data(void);
static void _course_clear_data(linked_list_t* item);

static linked_list_t ece_sem1;
static linked_list_t ece_sem2;

int main(void)
{
	student_info_t *temp;
	uint16_t total_students;
	students_print_details();

	printf("adding test data\r\n");
	_add_test_data();
	students_print_details();

	total_students = students_get_total();
	printf("total number of students:%d\n",total_students);

	printf("position of roll number %d is %d\n",5,students_get_position(5));

	_insert_data();
	students_print_details();

	temp = students_pop_by_position(3);
	if(temp == NULL)
		printf("could not get student from position 3\n");
	else{
		printf("removed student from position 3\n");
		free(temp);
		temp = NULL;
	}
	students_print_details();

	printf("clearing test data\r\n");
	_clear_data();
	students_print_details();

/////////////////////////////////////////

	_course_details(&ece_sem1);
	_course_details(&ece_sem2);

	return 0;
}

static void _add_test_data(void)
{
	bool result;

	student_info_t* student1;
	student1 = (student_info_t *)calloc(1, sizeof(student_info_t));

	student1->roll_number = 1;
	memcpy(student1->name, "john",4);
	//result = students_push(student1);
	result = students_push_front(student1);

	if(result == true)
		printf("successfully added student1 to the list\r\n");
	else
		printf("failed to add student1 to the list\r\n");

	student_info_t* student2;
	student2 = (student_info_t *)calloc(1, sizeof(student_info_t));

	student2->roll_number = 2;
	memcpy(student2->name, "peter",5);
	//result = students_push(student2);
	result = students_push_front(student2);

	if(result == true)
		printf("successfully added student2 to the list\r\n");
	else
		printf("failed to add student2 to the list\r\n");

	student_info_t* student3;
	student3 = (student_info_t *)calloc(1, sizeof(student_info_t));

	student3->roll_number = 3;
	memcpy(student3->name, "siddarth",8);
	result = students_push(student3);
	//result = students_push_front(student3);

	if(result == true)
		printf("successfully added student3 to the list\r\n");
	else
		printf("failed to add student3 to the list\r\n");

}

static void _insert_data(void)
{
	bool result;

//	position = 3;
	student_info_t* student4;
	student4 = (student_info_t *)calloc(1, sizeof(student_info_t));

	student4->roll_number = 4;
	memcpy(student4->name, "jack",4);
	result =students_insert(student4, 2);

	if(result == true)
		printf("successfully inserted student4 to the list\r\n");
	else
		printf("failed to insert student4 to the list\r\n");

}

static void _course_add_test_data(void)
{
	bool result;

/////////////////////////////////////////////ece_sem1////////////////
	course_info_t* sem1_course1;
	sem1_course1 = (course_info_t*)calloc(1, sizeof(course_info_t));

	sem1_course1->id = 10;
	memcpy(sem1_course1->name, "embedded systems",16);
	result = course_push(&ece_sem1,sem1_course1);

	if(result == true)
		printf("successfully added sem1_course1 details\r\n");
	else
		printf("failed to add sem1_course1 details\r\n");

	course_info_t* sem1_course2;
	sem1_course2 = (course_info_t *)calloc(1, sizeof(course_info_t));

	sem1_course2->id = 5;
	memcpy(sem1_course2->name, "python",6);
	result = course_push_to_front(&ece_sem1,sem1_course2);

	if(result == true)
		printf("successfully added sem1_course2 details\r\n");
	else
		printf("failed to add sem1_course2 details\r\n");

	course_info_t* sem1_course3;
	sem1_course3 = (course_info_t *)calloc(1, sizeof(course_info_t));

	sem1_course3->id = 25;
	memcpy(sem1_course3->name, "maths",5);
	result = course_insert(&ece_sem1,sem1_course3,1);

	if(result == true)
		printf("successfully inserted sem1_course3 details\r\n");
	else
		printf("failed to insert sem1_course3 details\r\n");

///////////////////////////////////////ece_sem2//////////////////////////


	course_info_t* sem2_course1;
	sem2_course1 = (course_info_t*)calloc(1, sizeof(course_info_t));

	sem2_course1->id = 100;
	memcpy(sem2_course1->name, "electronics",11);
	result = course_push(&ece_sem2,sem2_course1);

	if(result == true)
		printf("successfully added sem2_course1 details\r\n");
	else
		printf("failed to add sem2_course1 details\r\n");

	course_info_t* sem2_course2;
	sem2_course2 = (course_info_t *)calloc(1, sizeof(course_info_t));

	sem2_course2->id = 101;
	memcpy(sem2_course2->name, "DC",2);
	result = course_push_to_front(&ece_sem2,sem2_course2);

	if(result == true)
		printf("successfully added sem2_course2 details\r\n");
	else
		printf("failed to add sem2_course2 details\r\n");

	course_info_t* sem2_course3;
	sem2_course3 = (course_info_t *)calloc(1, sizeof(course_info_t));

	sem2_course3->id = 102;
	memcpy(sem2_course3->name, "DIP",3);
	result = course_insert(&ece_sem2,sem2_course3,2);

	if(result == true)
		printf("successfully inserted sem2_course3 details\r\n");
	else
		printf("failed to insert sem2_course3 details\r\n");
}

static void _course_details(linked_list_t* list)
{
	if(list == NULL)
		return;

	uint16_t total_courses;
	course_info_t* temp1;
	course_init(list);

	course_print_details(list);
	printf("adding test data\r\n");
	_course_add_test_data();
	course_print_details(list);

	total_courses = course_get_size(list);
	printf("total number of courses:%d\r\n",total_courses);

	temp1 = course_pop_by_position(list,2);
	if(temp1 == NULL)
		printf("could not get course from position 2\n");
	else{
		printf("removed course from position 2\n");
		free(temp1);
		temp1 = NULL;
	}
	course_print_details(list);

	printf("clearing test data\r\n");
	_course_clear_data(list);
	course_print_details(list);
}

static void _clear_data(void)
{
	student_info_t* temp;

	temp = students_peek();//get first element

	while (temp != NULL) {
		students_pop();//remove first element
		free(temp);    //free up allocated memory
		temp = students_peek();
	}
}

static void _course_clear_data(linked_list_t* item)
{
	if(item == NULL)
		return;

	course_info_t* temp;
	temp = course_peek(item);

	while(temp != NULL){
		course_pop(item);
		free(temp);
		temp = course_peek(item);
	}
}
