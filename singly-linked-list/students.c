#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "students.h"

static student_info_t *_head;
static student_info_t *_tail;
static uint16_t _total_students;
static student_info_t* _get_element_by_position(uint16_t position);

//static student_info_t* _get_tail_element(void);

bool students_push(student_info_t *new_student)
{
	//add elements to the end of the list

	//student_info_t *tail;

	if(new_student == NULL)
		return false;

	if(_head == NULL){
		//list is empty
		_head = new_student;
		//_total_students++;
	}
	else
	{
		//list is not empty
		_tail->next = new_student;
		//tail = _get_tail_element();
		//tail->next = new_student;
	}
	new_student->next = NULL;
	_tail = new_student;
	_total_students++;

	return true;
}

bool students_push_front(student_info_t *new_student)
{
	if(new_student == NULL)
			return false;

	if(_head == NULL) {
		_head = new_student;
		new_student->next = NULL;
		_tail = new_student;
	}

	else {
		new_student->next = _head;
		_head = new_student;
	}
	_total_students++;

	return true;
}
bool students_insert(student_info_t* new_student,uint16_t position)
{
	student_info_t *prev;

	if(new_student == NULL)
		return false;

	if(position == 0)
		return false;

	if(position == 1)
		return students_push_front(new_student);

	prev = _get_element_by_position(position-1);
	if(prev == NULL){
		return students_push(new_student);
	}
	new_student->next = prev->next;
	prev->next = new_student;
	_total_students++;

	return true;
}

student_info_t* students_pop(void)
{	//returns and removes first element
	student_info_t* first;

	if(_head == NULL)
		return NULL;
	else { //list is not empty
		first = _head;
		_head = _head->next;
		_total_students--;

		if(_head == NULL)
			_tail = NULL;

		first->next = NULL;
		return first;
	}
	return NULL;
}
student_info_t* students_pop_by_position(uint16_t position)
{
	student_info_t* prev;
	student_info_t* temp;

	if(position == 0)
		return NULL;

	if(position == 1)
		return students_pop();

	if(position > _total_students)
		return NULL;

	prev = _get_element_by_position(position-1);
	if(prev == NULL){
		return NULL;
	}
	temp = prev->next;
	prev->next = temp->next;

	temp->next = NULL;//invalidate

	if(position == _total_students)//last element popped
			_tail = prev;

	return temp;
}
student_info_t* students_peek(void)
{	//returns first element

	return _head;
}

uint16_t students_get_total(void)
{
//	student_info_t *temp;
//	uint16_t total_students;
//
//	temp = _head;
//	total_students = 0;
//
//	while(temp != NULL) {
//		total_students++;
//		temp = temp->next;
//	}
	return _total_students;
}

void students_print_details(void)
{
	student_info_t *temp;
	uint16_t total_students;

	temp = _head;
	total_students = 0;

	while(temp != NULL) {
		printf("%6d, %s\r\n",temp->roll_number,temp->name);
		total_students++;
		temp = temp->next;
	}
	printf("total students:%d\r\n",total_students);
}
uint16_t students_get_position(uint16_t roll_number)
{
	student_info_t *temp;
	uint16_t position;

	temp = _head;
	position = 1;

	while(temp != NULL) {
		if(temp->roll_number == roll_number)
			return position;

		temp = temp->next;
		position++;
	}
	return 0;
}

static student_info_t* _get_element_by_position(uint16_t position)
{
	uint16_t count;
	student_info_t *temp;

	if(position == 1)
		return _head;

	temp = _head;
	count = 1;
	while(temp != NULL){
		if(count == position)
			return temp;

		count++;
		temp = temp->next;
	}

	return NULL;
}
//static student_info_t* _get_tail_element(void)
//{
//	student_info_t *temp;
//
//	if(_head == NULL) {
//		return NULL;
//	}
//	else {
//		temp = _head;
//		while(temp->next != NULL)
//			temp = temp->next;
//		return temp;
//	}
//}
