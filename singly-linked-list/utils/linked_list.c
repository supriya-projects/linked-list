#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "linked_list.h"

#define INIT_MAGIC_NUMBER 0xDEADBEEF

static linked_list_item_t* _get_element_by_position(linked_list_t* list,uint16_t position);

void linked_list_init(linked_list_t* list)
{
	if(!list)
		return;

	list->head = NULL;
	list->tail = NULL;
	list->size = 0;
	list->init_magic_number = INIT_MAGIC_NUMBER;
}

bool linked_list_push(linked_list_t* list,linked_list_item_t* item)
{
	if(!list || !item)
		return false;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	if(list->head == NULL){ 	//list is empty
		list->head = item;
	}
	else
	{
		list->tail->next = item;
	}
	list->tail = item;
	item->next = NULL;
	list->size++;

	return true;
}

bool linked_list_push_to_front(linked_list_t* list, linked_list_item_t* item)
{
	if(!list || !item)
		return false;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	if(list->head == NULL) {
		list->head = item;
		item->next = NULL;
		list->tail = item;
	}

	else {
		item->next = list->head;
		list->head = item;
	}
	list->size++;

	return true;
}

bool linked_list_insert(linked_list_t* list, linked_list_item_t* item,uint16_t position)
{
	linked_list_item_t *prev;

	if(!list || !item)
		return false;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	if(position == 0)
		return false;

	if(position == 1)
		return linked_list_push_to_front(list,item);

	if(position > list->size)
		return linked_list_push(list, item);

	prev = _get_element_by_position(list,position-1);
	if(prev == NULL){
		return linked_list_push(list,item);
	}
	item->next = prev->next;
	prev->next = item;
	list->size++;

	return true;
}

linked_list_item_t* linked_list_pop(linked_list_t* list)
{
	linked_list_item_t* first;

	if(list == NULL)
		return NULL;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	if(list->head == NULL)
		return 	NULL;

	first = list->head;
	list->head = list->head->next;
	list->size--;

	if(list->head == NULL)
		list->tail = NULL;

	first->next = NULL;
	return first;

}

linked_list_item_t* linked_list_pop_by_position(linked_list_t* list,uint16_t position)
{
	linked_list_item_t* prev;
	linked_list_item_t* temp;

	if(list == NULL)
		return NULL;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	if(position == 0)
		return NULL;

	if(position == 1)
		return linked_list_pop(list);

	if(position > list->size)
		return NULL;

	prev = _get_element_by_position(list,position-1);
	if(prev == NULL){
		return NULL;
	}
	temp = prev->next;
	prev->next = temp->next;

	temp->next = NULL;//invalidate

	if(position == list->size)//last element popped
		list->tail = prev;

	list->size--;

	return temp;
}

linked_list_item_t* linked_list_peek(linked_list_t* list)
{
	if(!list)
		return NULL;

	if(list->init_magic_number != INIT_MAGIC_NUMBER)
		return false;

	return list->head;
}

void linked_list_print_details(linked_list_t *list,item_print_t fn_item_print)
{
	linked_list_item_t* temp;

	if(!list)
		return;

	if(list->init_magic_number != INIT_MAGIC_NUMBER){
		printf("linked list not initialized\n");
		return;
	}

	temp = list->head;

	while(temp != NULL){
		if(fn_item_print){
			fn_item_print(temp->parent);
		}
		temp = temp->next;
	}

}

static linked_list_item_t* _get_element_by_position(linked_list_t* list,uint16_t position)
{
	uint16_t count;
	linked_list_item_t* temp;

	if(position == 1)
		return list->head;

	temp = list->head;
	count = 1;
	while(temp != NULL){
		if(count == position)
			return temp;

		count++;
		temp = temp->next;
	}

	return NULL;
}
