#ifndef UTILS_LINKED_LIST_H_
#define UTILS_LINKED_LIST_H_

#include <stdbool.h>
#include <stdint.h>

typedef void (*item_print_t)(void* parent);

typedef struct __linked_list_item_ {
	struct __linked_list_item_* next;
	void* parent;
}linked_list_item_t;

typedef struct __linked_list_{
	linked_list_item_t* head;
	linked_list_item_t* tail;
	uint16_t size;
	uint32_t init_magic_number;
}linked_list_t;

void linked_list_init(linked_list_t* list);
bool linked_list_push(linked_list_t* list, linked_list_item_t* item);
bool linked_list_push_to_front(linked_list_t* list, linked_list_item_t* item);
bool linked_list_insert(linked_list_t* list, linked_list_item_t* item,uint16_t position);

linked_list_item_t* linked_list_pop(linked_list_t* list);
linked_list_item_t* linked_list_pop_by_position(linked_list_t* list,uint16_t position);
linked_list_item_t* linked_list_peek(linked_list_t* list);

void linked_list_print_details(linked_list_t *list,item_print_t fn_item_print);
#endif /* UTILS_LINKED_LIST_H_ */
