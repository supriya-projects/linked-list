#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "course.h"
#include "utils/linked_list.h"

//static linked_list_t _list;//remove this
static void _print_item_details(void* parent);

void course_init(linked_list_t* list)
{
	if(list == NULL)
		return;

	linked_list_init(list);
}

bool course_push(linked_list_t* list,course_info_t *course)
{
	if(list == NULL)
		return false;

	if(!course)
		return false;

	course->item.parent = (void *)course;

	return linked_list_push(list, &course->item);

}

bool course_push_to_front(linked_list_t* list,course_info_t *course)
{
	if(list == NULL)
		return false;

	if(!course)
		return false;

	course->item.parent = (void *)course;

	return linked_list_push_to_front(list, &course->item);

}

bool course_insert(linked_list_t* list,course_info_t *course,uint16_t position)
{
	if(list == NULL)
		return false;

	if(!course)
		return false;

	course->item.parent = (void *)course;

	return linked_list_insert(list, &course->item, position);

}


course_info_t* course_pop(linked_list_t* list)
{
	if(list == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_pop(list);
	if(item == NULL)
		return NULL;

	return(course_info_t*)item->parent;
}

course_info_t* course_pop_by_position(linked_list_t* list,uint16_t position)
{
	if(list == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_pop_by_position(list,position);
	if(item == NULL)
		return NULL;

	return(course_info_t*)item->parent;
}

course_info_t* course_peek(linked_list_t* list)
{
	if(list == NULL)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_peek(list);
	if(item == NULL)
		return NULL;

	return(course_info_t*)item->parent;
}

uint16_t course_get_size(linked_list_t* list)
{
	if(list == NULL)
		return 0;

	return list->size;
}

void course_print_details(linked_list_t* list)
{
	if(list == NULL)
		return;

	linked_list_print_details(list, _print_item_details);
//
//	course_info_t *info;
//	linked_list_item_t *temp;
//
//	temp = _list.head;
//	_list.size = 0;
//
//	info = (course_info_t*)temp->parent;
//
//	while(temp != NULL) {
//		printf("%6d, %s\r\n", info->id, info->name);
//		_list.size++;
//		temp = temp->next;
//	}
//	printf("total :%d\r\n",_list.size);
}

static void _print_item_details(void* parent)
{
	course_info_t* temp;

	if(!parent)
		return;

	temp = (course_info_t*)parent;
	printf("%4d, %s\n",temp->id,temp->name);

}
