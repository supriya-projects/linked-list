#ifndef STUDENTS_H_
#define STUDENTS_H_

#include<stdbool.h>
#include<stdint.h>

#define NAME_MAX_LEN_BYTES 200

typedef struct __student_info_ {
	uint16_t roll_number;
	uint8_t name[NAME_MAX_LEN_BYTES];
	struct __student_info_ *next;
} student_info_t;

bool students_push(student_info_t *new_student);
bool students_push_front(student_info_t *new_student);
bool students_insert(student_info_t *new_student,uint16_t position);
student_info_t* students_pop(void);
student_info_t* students_pop_by_position(uint16_t position);
student_info_t* students_peek(void);
uint16_t students_get_total(void);
uint16_t students_get_position(uint16_t roll_number);

//utility function
void students_print_details(void);

#endif /* STUDENTS_H_ */
