#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "array.h"

int main(void)
{
	array_info_t *temp;

	temp->arr_size = 10;
	uint8_t array[] = {1,2,3,4,5,6,7,8};
	temp->len = 8;
	bool result;

	array_print_details(array);

	printf("adding element at last\n");
	result = array_push(array,9);
	array_print_details(array);

	if(result == true)
		printf("successfully added element at last\n");
	else
		printf("failed to add element at last\n");

	printf("adding element at first\n");
	result = array_push_to_front(array, 9);
	array_print_details(array);

	if(result == true)
		printf("successfully added element at first\n");
	else
		printf("failed to add element at first\n");

	printf("removing first element\n");
	uint8_t val = array_pop(array,&val);
	array_print_details(array);

	if(result == true)
		printf("successfully removed element at first\n");
	else
		printf("failed to remove element at first\n");

	printf("removing element at given position\n");
	uint8_t value = array_pop_by_position(array,&value, 5);
	array_print_details(array);

	if(result == true)
		printf("successfully removed element\n");
	else
		printf("failed to remove element\n");

	return 0;
}

