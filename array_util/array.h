#ifndef ARRAY_H_
#define ARRAY_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct __array_info_ {
	uint8_t* array;
	uint16_t arr_size;
	uint16_t len;
}array_info_t;

bool array_push(array_info_t*,uint8_t new_element);
bool array_push_to_front(array_info_t*,uint8_t new_element);

bool array_pop(array_info_t*, uint8_t *return_element);
bool array_pop_by_position(array_info_t*, uint8_t *return_element,uint16_t position);

void array_print_details(array_info_t*);

#endif /* ARRAY_H_ */
