#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "array.h"

static array_info_t* temp;

bool array_push(array_info_t* array,uint8_t new_element)
{
	if(temp->array == NULL)
		return false;

	temp->array[temp->len-1] = new_element;
//	temp->array[temp->arr_size-1] = new_element;

	return true;
}

bool array_push_to_front(array_info_t* array,uint8_t new_element)
{
	uint8_t i=0;
	if(temp->array == NULL)
		return false;

	for(i=temp->arr_size-1;i>0;i--){
		temp->array[i] = temp->array[i-1];
	}

	temp->array[i] = new_element;

	return true;
}
bool array_pop(array_info_t* array, uint8_t *return_element)
{
	uint8_t i=0 ;
	if(temp->array == NULL)
		return false;

	if(return_element == NULL)
		return false;

	uint8_t val;
	val = temp->array[i];

	for(i=0; i<temp->arr_size-1; i++){
		temp->array[i] = temp->array[i+1];
	}

	return val;

	return true;
}
bool array_pop_by_position(array_info_t* array, uint8_t *return_element,uint16_t position)
{
	uint8_t i=0 ;
	if(temp->array == NULL)
		return false;

	if(return_element == NULL)
		return false;

	if(position > temp->arr_size)
		return false;

	uint8_t val;
	val = temp->array[position-1];

	for(i=position-1; i<temp->arr_size-1; i++){
		temp->array[i] = temp->array[i+1];
	}
	return val;

	return true;
}

void array_print_details(array_info_t* array)
{
	uint8_t i=0;

	for(i=0;i<temp->arr_size;i++){
		printf("%d",temp->array[i]);
	}
	printf("\n");
}
