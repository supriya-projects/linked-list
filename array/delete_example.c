#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>

bool delete_element(uint8_t *array, uint8_t arr_size, uint8_t pos);

int main(void)
{
        uint8_t array[] = {1,2,3,4,5,6,7,8,9};
        uint8_t i,arr_size,pos;
        pos = 6;

        arr_size=sizeof(array)/sizeof(array[0]);

        printf("original array:\n");
        for(i=0;i<arr_size;i++){
                printf(" %d",array[i]);
        }
        printf("\n");

        delete_element(array,arr_size,pos);

        printf("deleted position %d\n",pos);
	
	printf("updated array:\n");
       	for(i=0;i<arr_size-1;i++){
               	printf(" %d",array[i]);
       	}
       	printf("\n");

        return 0;
}

bool delete_element(uint8_t *array, uint8_t arr_size, uint8_t pos)
{
	if( array == NULL )
		return false;

	if( arr_size == 0 )
		return false;

	if( pos > arr_size )
		return false;

	uint8_t i;

	for(i = pos; i < arr_size - 1; i++)
	{
		array[i] = array[i+1];
	}

	return true;
}
