#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>

bool add_new_element(uint8_t *array, uint8_t arr_size, uint8_t pos, uint8_t new_element); 

int main(void)
{
	uint8_t array[] = {1,2,3,4,5,6,7,8,9};
	uint8_t i,arr_size,pos,new_element;
	pos = 7;
	new_element = 15;

	arr_size=sizeof(array)/sizeof(array[0]);

	printf("original array:\n");
        for(i=0;i<arr_size;i++){
                printf(" %d",array[i]);
	}
	printf("\n");

	add_new_element(array,arr_size,pos,new_element);
	
	printf("new element value %d at position %d\n",new_element,pos);
	
	printf("updated array:\n");	
	for(i=0;i<arr_size;i++){
		printf(" %d",array[i]);
	}
	printf("\n");

	return 0;
}

bool add_new_element(uint8_t *array, uint8_t arr_size, uint8_t pos, uint8_t new_element)
{
	if ( array == NULL )
		return false;
	
	if ( arr_size == 0 )
		return false;
	
	if ( pos > arr_size )
		return false;

	uint8_t i;

	for(i = arr_size - 1; i >= pos ; i--)
	{
		array[i] = array[i-1];
	}
	array[pos-1] = new_element;
	
	return true;
}


