#ifndef COURSE_H_
#define COURSE_H_

#include <stdint.h>
#include <stdbool.h>

#define NAME_MAX_LEN_BYTES 200

typedef struct __course_details_ {
	uint8_t course_name[NAME_MAX_LEN_BYTES];
	uint16_t credit_hours;
	uint16_t number_of_students;
	struct __course_details_ *next;
} course_details_t;

bool course_push(course_details_t *new_course);
bool course_push_front(course_details_t *new_course);
bool course_insert(course_details_t *new_course,uint16_t position);
course_details_t* course_pop(void);
course_details_t* courses_pop_by_position(uint16_t position);
course_details_t* course_peek(void);
uint16_t courses_get_total(void);

void course_print_details(void);

#endif /* COURSE_H_ */
