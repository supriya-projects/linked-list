#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "course.h"

static void _add_test_data(void);
static void _clear_data(void);

int main(void)
{
	uint16_t total_courses,pos;
	course_details_t* temp;

	course_print_details();

	printf("adding test data\r\n");
	_add_test_data();
	course_print_details();

	total_courses = courses_get_total();
	printf("total number of courses:%d\r\n",total_courses);

	pos = 2;
	temp = courses_pop_by_position(pos);
	if(temp == NULL)
		printf("could not get course from position %d\n",pos);
	else{
		printf("removed course from position %d\n",pos);
		free(temp);
		temp = NULL;
	}
	course_print_details();


	printf("clearing test data\r\n");
	_clear_data();
	course_print_details();

}
static void _add_test_data(void)
{
	bool result;

	course_details_t* course1;
	course1 = (course_details_t *)calloc(1, sizeof(course_details_t));

	course1->credit_hours = 730;
	course1->number_of_students = 10;
	memcpy(course1->course_name, "embedded systems",16);
	result = course_push(course1);

	if(result == true)
			printf("successfully added course1 details\r\n");
		else
			printf("failed to add course1 details\r\n");

	course_details_t* course2;
	course2 = (course_details_t *)calloc(1, sizeof(course_details_t));

	course2->credit_hours = 730;
	course2->number_of_students = 5;
	memcpy(course2->course_name, "python",6);
	result = course_push_front(course2);

	if(result == true)
		printf("successfully added course2 details\r\n");
	else
		printf("failed to add course2 details\r\n");

	course_details_t* course3;
	course3 = (course_details_t *)calloc(1, sizeof(course_details_t));

	course3->credit_hours = 480;
	course3->number_of_students = 15;
	memcpy(course3->course_name, "java",4);
	result = course_insert(course3, 2);

	if(result == true)
		printf("successfully added course3 details\r\n");
	else
		printf("failed to add course3 details\r\n");

}
static void _clear_data(void)
{
	course_details_t* temp;
	temp = course_peek();

	while(temp != NULL){
		course_pop();
		free(temp);
		temp = course_peek();
	}
}
