#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "course.h"

static course_details_t *_head;
static course_details_t *_tail;
static uint16_t _total_courses;
static course_details_t* _get_element_by_position(uint16_t position);

bool course_push(course_details_t *new_course)
{
	if(new_course == NULL)
		return false;

	if(_head == NULL) {
		_head = new_course;
	}
	else
	{
		_tail->next = new_course;
	}
	_tail = new_course;
	new_course->next = NULL;
	_total_courses++;

	return true;
}
bool course_push_front(course_details_t *new_course)
{
	if(new_course == NULL)
		return false;

	if(_head == NULL){
		_head = new_course;
		_head->next = NULL;
		_tail = new_course;
	}
	else
	{
		new_course->next = _head;
		_head = new_course;
	}
	_total_courses++;

	return true;
}
bool course_insert(course_details_t *new_course,uint16_t position)
{
	course_details_t* prev;

	if (new_course == NULL)
		return false;

	if(position == 0)
		return false;

	if(position == 1)
		return course_push_front(new_course);

	prev = _get_element_by_position(position-1);

	if(prev == NULL)
		return course_push(new_course);

	new_course->next = prev->next;
	prev->next = new_course;
	_total_courses++;

	return true;
}
course_details_t* course_pop(void)
{
	course_details_t *first;

	if(_head == NULL)
		return NULL;
	else
	{
		first = _head;
		_head = _head->next;
		_total_courses--;

		if(_head == NULL)
			_tail = NULL;

		first->next = NULL;
		return first;
	}
	return NULL;
}
course_details_t* courses_pop_by_position(uint16_t position)
{
	course_details_t* prev;
	course_details_t* temp;

	if(position == 0)
		return NULL;

	if(position == 1)
		return course_pop();

	if(position > _total_courses)
		return NULL;

	prev = _get_element_by_position(position-1);
	if(prev == NULL){
		return NULL;
	}
	temp = prev->next;
	prev->next = temp->next;

	temp->next = NULL;//invalidate

	if(position == _total_courses)//last element popped
			_tail = prev;

	return temp;
}
course_details_t* course_peek(void)
{
	return _head;
}
uint16_t courses_get_total(void)
{
	return _total_courses;
}
void course_print_details(void)
{
	course_details_t *temp;
	uint16_t total_courses;

	temp = _head;
	total_courses = 0;

	while(temp != NULL)
	{
		printf("course name: %s,credit hours:%6d,number of students:%6d \r\n", temp->course_name, temp->credit_hours, temp->number_of_students);
		total_courses++;
		temp = temp->next;
	}
	printf("total courses:%d\r\n",total_courses);
}
static course_details_t* _get_element_by_position(uint16_t position)
{
	course_details_t* temp;
	uint16_t count;

	if(position == 1)
		return _head;

	temp = _head;
	count = 1;

	while(temp != NULL){
		if(count == position)
			return temp;

		count++;
		temp = temp->next;
	}
	return NULL;
}
