#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "books.h"

static book_info_t *_head;
static book_info_t *_tail;
static uint16_t _total_books;

bool books_push(book_info_t *new_book)
{
	if (new_book == NULL)
		return false;

	if (_head == NULL) {
		_head = new_book;
	}
	else {
		_tail->next = new_book;
	}
	new_book->next = NULL;
	_tail = new_book;
	_total_books++;

	return true;

}
book_info_t* books_pop(void)
{
	book_info_t *first;

	if(_head == NULL)
		return NULL;
	else {
		first = _head;
		_head = _head->next;
		_total_books--;

		if(_head == NULL)
			_tail = NULL;

		first->next = NULL;
		return first;
	}
	return NULL;
}
book_info_t* books_peek(void)
{
	return _head;
}
uint16_t books_get_total(void)
{
	return _total_books;
}
void books_print_details(void)
{
	book_info_t *temp;
	uint16_t total_books;

	temp = _head;
	total_books = 0;

	while(temp != NULL) {
		printf("%6d, %s\r\n",temp->book_number,temp->name);
		total_books++;
		temp = temp->next;
	}

	printf("total books:%d\r\n",total_books);
}
