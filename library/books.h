#ifndef BOOKS_H_
#define BOOKS_H_

#include <stdint.h>
#include <stdbool.h>

#define NAME_MAX_LEN_BYTES 200

typedef struct __book_info_ {
	uint16_t book_number;
	uint8_t name[NAME_MAX_LEN_BYTES];
	struct __book_info_ *next;
} book_info_t;

bool books_push(book_info_t *new_book);
book_info_t* books_pop(void);
book_info_t* books_peek(void);
uint16_t books_get_total(void);

//utility function
void books_print_details(void);
#endif
