#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "books.h"

static void _add_test_data(void);
static void _clear_data(void);

int main(void)
{
	uint16_t total_books;

	books_print_details();

	printf("adding test data\r\n");
	_add_test_data();
	books_print_details();

	total_books = books_get_total();
	printf("total number of books:%d\r\n",total_books);

	printf("clearing test data\r\n");
	_clear_data();
	books_print_details();

	return 0;
}
static void _add_test_data(void)
{
	bool result;

	book_info_t* book1;
	book1 = (book_info_t *)calloc(1, sizeof(book_info_t));

	book1->book_number = 1;
	memcpy(book1->name, "maths",5);
	result = books_push(book1);

	if(result == true)
		printf("successfully added book1 to the list\r\n");
	else
		printf("failed to add book1 to the list\r\n");

	book_info_t* book2;
	book2 = (book_info_t *)calloc(1, sizeof(book_info_t));

	book2->book_number = 2;
	memcpy(book2->name, "physics", 7);
	result = books_push(book2);

	if(result == true)
		printf("successfully added book2 to the list\r\n");
	else
		printf("failed to add book2 to the list\r\n");
}
static void _clear_data(void)
{
	book_info_t *temp;
	temp = books_peek();

	while(temp != NULL)
	{
		books_pop();
		free(temp);
		temp = books_peek();
	}
}
